/*
 * 
 */
package eg.edu.alexu.csd.oop.game.snapshot;

import eg.edu.alexu.csd.oop.game.GameObject;

/**
 * The Class Memento.
 */
public class Memento {

	/** The game object. */
	private GameObject gameObject;
	
	/** The x pos. */
	private int xPos;
	
	/** The score. */
	private int score;
	
	/** The right stack. */
	private int rightStack;
	
	/** The left stack. */
	private int leftStack;

	/**
	 * Instantiates a new memento.
	 *
	 * @param gameObject the game object
	 */
	public Memento(GameObject gameObject) {
		this.gameObject = gameObject;
	}

	/**
	 * Gets the xpos.
	 *
	 * @return the xpos
	 */
	public int getXpos() {
		return this.xPos;
	}

	/**
	 * Sets the left.
	 *
	 * @param left the new left
	 */
	public void setLeft(int left) {
		this.leftStack = left;
	}

	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Sets the score.
	 *
	 * @param score the new score
	 */
	public void setScore(int score) {
		this.score = score;
	}

	/**
	 * Gets the left.
	 *
	 * @return the left
	 */
	public int getLeft() {
		return this.leftStack;
	}

	/**
	 * Gets the right.
	 *
	 * @return the right
	 */
	public int getRight() {
		return this.rightStack;
	}

	/**
	 * Sets the right.
	 *
	 * @param right the new right
	 */
	public void setRight(int right) {
		this.rightStack = right;
	}

	/**
	 * Gets the.
	 *
	 * @return the game object
	 */
	public GameObject get() {
		return this.gameObject;
	}

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	public void setX(int x) {
		this.xPos = x;
	}

}
