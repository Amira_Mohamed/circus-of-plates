/*
 * 
 */
package eg.edu.alexu.csd.oop.game;

// TODO: Auto-generated Javadoc
/**
 * The Interface GameObject.
 */
public interface GameObject {
	
	/**
	 *  setter/getter for X position.
	 *
	 * @return the x
	 */
	int getX();

	/**
	 * Sets the x.
	 *
	 * @param x the new x
	 */
	void setX(int x);

	/**
	 *  setter/getter for Y position.
	 *
	 * @return the y
	 */
	int getY();

	/**
	 * Sets the y.
	 *
	 * @param y the new y
	 */
	void setY(int y);

	/**
	 * Gets the width.
	 *
	 * @return object width
	 */
	int getWidth();

	/**
	 * Gets the height.
	 *
	 * @return object height
	 */
	int getHeight();

	/**
	 * Checks if is visible.
	 *
	 * @return object visible or not
	 */
	boolean isVisible();

	/**
	 * Gets the sprite images.
	 *
	 * @return object movement frames
	 */
	java.awt.image.BufferedImage[] getSpriteImages();
}