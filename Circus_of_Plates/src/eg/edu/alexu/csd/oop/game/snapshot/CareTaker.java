/*
 * 
 */
package eg.edu.alexu.csd.oop.game.snapshot;

import java.util.LinkedList;

/**
 * The Class CareTaker.
 */
public class CareTaker {

	/** The control. */
	protected LinkedList<Memento> control = new LinkedList<Memento>();

	/**
	 * Save.
	 *
	 * @param memento the memento
	 */
	public void save(Memento memento) {
		control.add(memento);
	}

	/**
	 * Load.
	 *
	 * @return the linked list
	 */
	public LinkedList<Memento> load() {
		return control;
	}

}
