/*
 * 
 */
package eg.edu.alexu.csd.oop.game.iterator;

import java.util.List;

import eg.edu.alexu.csd.oop.game.GameObject;

/**
 * The Class IteratorImpl.
 */
public class IteratorImpl implements Iterator {

	/** The list. */
	private GameObject[] list;
	
	/** The counter. */
	private int counter;
	
	/** The length. */
	private int length;

	/**
	 * Instantiates a new iterator impl.
	 *
	 * @param list the list
	 */
	public IteratorImpl(List<GameObject> list) {
		this.length = list.size();
		this.list = new GameObject[this.length];
		this.list = list.toArray(this.list);
		this.counter = 0;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.iterator.Iterator#hasNext()
	 */
	@Override
	public boolean hasNext() {
		return (counter) < length;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.iterator.Iterator#next()
	 */
	@Override
	public GameObject next() {
		return this.list[counter++];
	}

}
