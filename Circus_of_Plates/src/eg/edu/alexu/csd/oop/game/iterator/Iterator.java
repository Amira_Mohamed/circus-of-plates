/*
 * 
 */
package eg.edu.alexu.csd.oop.game.iterator;

import eg.edu.alexu.csd.oop.game.GameObject;

/**
 * The Interface Iterator.
 */
public interface Iterator {

	/**
	 * Checks for next.
	 *
	 * @return true, if successful
	 */
	public boolean hasNext();

	/**
	 * Next.
	 *
	 * @return the game object
	 */
	public GameObject next();

}
