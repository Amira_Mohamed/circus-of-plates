/*
 * 
 */
package eg.edu.alexu.csd.oop.game.factory;

import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.World;

/**
 * The Class Factory.
 */
public class Factory {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Factory.class);

	/**
	 * Gets the state object.
	 *
	 * @param className the class name
	 * @param path the path
	 * @return the state object
	 */
	public World getStateObject(String className, String path) {
		try {
			try {
				return (World) (Class.forName("eg.edu.alexu.csd.oop.game.strategy." + className))
						.getConstructor(String.class).newInstance(path);
			} catch (IllegalArgumentException | InvocationTargetException | NoSuchMethodException
					| SecurityException e) {
				e.printStackTrace();
				logger.fatal(">> Strategy can't be loaded " + e);
			}
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			logger.fatal(">> Strategy can't be loaded " + e);
		}
		return null;
	}
}
