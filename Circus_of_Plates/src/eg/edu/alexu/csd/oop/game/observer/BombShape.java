/*
 * 
 */
package eg.edu.alexu.csd.oop.game.observer;

import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.dynamiclinkage.Shape;
import eg.edu.alexu.csd.oop.game.strategy.WorldImpl;

/**
 * The Class BombShape.
 */
public class BombShape extends Shape implements Observer {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(BombShape.class);

	/**
	 * Instantiates a new bomb shape.
	 */
	public BombShape() {
		this.setX(new Random().nextInt(WorldImpl.WIDTH));
		this.setY(0);
		try {
			this.spriteImages[0] = ImageIO.read(getClass().getResourceAsStream("/bombCool.png"));
			logger.info(">> Picture is Loaded Successfully");
		} catch (IOException e) {
			logger.fatal(">> Picture Can't be loaded " + e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see eg.edu.alexu.csd.oop.game.dynamiclinkage.Shape#update()
	 */
	@Override
	public void update() {
		this.visible = !visible;
	}

}
