/*
 * 
 */
package eg.edu.alexu.csd.oop.game.strategy;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.iterator.IteratorImpl;
import eg.edu.alexu.csd.oop.game.objects.CharacterIF;

/**
 * The Class Easy.
 */
public class Easy extends WorldImpl {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Easy.class);

	/**
	 * Instantiates a new easy.
	 *
	 * @param path the path
	 */
	public Easy(String path) {
		super();
		run(path);
		setBarShapes();
		this.bar1 = constant.get(0);
		this.bar2 = constant.get(1);
		super.targetScore = 20;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#refresh()
	 */
	@Override
	public boolean refresh() {
		long now = System.currentTimeMillis();
		boolean clownReachedBar = true;
		List<GameObject> moveableToControl = new ArrayList<GameObject>();
		List<GameObject> removedMovingShapes = new ArrayList<GameObject>();
		try {
			iterator = new IteratorImpl(moving);
			logger.debug(">> Iterator Object to iterate over Moving GameObjects is created Successfully");
		} catch (RuntimeException e) {
			logger.fatal(">> Iterator Object can't be Created");
		}

		while (iterator.hasNext()) {
			GameObject object = iterator.next();
			GameObject bar = IntersectBar(object);
			if (bar != null) {
				if (isLeft(bar)) {
					object.setX(object.getX() + 1);
					if (IntersectBar(object) == null) {
						moving.add(pool.getShape(randomShapeGenerator[new Random().nextInt(4)]));
					}
				} else {
					object.setX(object.getX() - 1);
					if (IntersectBar(object) == null) {
						moving.add(pool.getShape(randomShapeGenerator[new Random().nextInt(4)]));
					}
				}
			} else {
				CharacterIF clown = intersectClown(object);
				if (clown == null) {
					object.setY(object.getY() + 4);
					if (object.getY() > HEIGHT) {
						super.pool.releaseShape(object);
						removedMovingShapes.add(object);
					}
				} else {
					if (getClownLeftHand(clown).intersects(getShape(object))) {
						clown.getCareTaker().saveLeft(clown.getOriginator().save(object));
						LinkedList<GameObject> list = clown.getCareTaker().checkScore("left");
						if (list.size() != 0) {
							super.score++;
							clown.setLeftStackYPosition(clown.getLeftStackYPosition() + 2 * object.getHeight());
							for (GameObject dummy : list) {
								if (dummy != object) {
									super.pool.releaseShape(dummy);
									super.control.remove(dummy);
								}
							}
							super.pool.releaseShape(object);
							removedMovingShapes.add(object);
						} else {
							clown.setLeftStackYPosition(clown.getLeftStackYPosition() - object.getHeight());
							moveableToControl.add(object);
						}
					} else if (getClownRightHand(clown).intersects(getShape(object))) {
						clown.getCareTaker().saveRight(clown.getOriginator().save(object));
						LinkedList<GameObject> list = clown.getCareTaker().checkScore("right");
						if (list.size() != 0) {
							super.score++;
							clown.setRightStackYPosition(clown.getRightStackYPosition() + 2 * object.getHeight());
							for (GameObject dummy : list) {
								if (dummy != object) {
									super.pool.releaseShape(dummy);
									super.control.remove(dummy);
								}
							}
							super.pool.releaseShape(object);
							removedMovingShapes.add(object);
						} else {
							clown.setRightStackYPosition(clown.getRightStackYPosition() - object.getHeight());
							moveableToControl.add(object);
						}
					}
				}
			}
		}
		for (GameObject object : moveableToControl) {
			super.control.add(object);
			super.moving.remove(object);
		}
		for (GameObject object : removedMovingShapes) {
			moving.remove(object);
		}
		for (int i = 0; i < super.clownsNumber; i++) {
			clownReachedBar = clownReachedBar && super.reachBar(super.control.get(i));
		}

		if (super.score == targetScore) {
			super.playerState = "Winner";
		}
		if ((now - super.startTime < MAX_TIME) && !(clownReachedBar)) {
			return true;
		} else {
			super.playerState = "Looser";
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#getSpeed()
	 */
	@Override
	public int getSpeed() {
		return 25;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#getControlSpeed()
	 */
	@Override
	public int getControlSpeed() {
		return 25;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#getStatus()
	 */
	@Override
	public String getStatus() {
		return "Score = " + this.score + "   | |   Target Score = " + targetScore + "   | |   Timer = "
				+ (System.currentTimeMillis() - startTime) / 1000 + " From " + MAX_TIME / 1000 + "   "
				+ super.playerState;
	}
}
