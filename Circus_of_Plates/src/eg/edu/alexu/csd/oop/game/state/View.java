/*
 * 
 */
package eg.edu.alexu.csd.oop.game.state;

/**
 * The Class View.
 */
public class View {

	/** The state. */
	private State state = null;

	/**
	 * Sets the state.
	 *
	 * @param state the new state
	 */
	public void setState(State state) {
		this.state = state;
	}

	/**
	 * Start.
	 */
	public void start() {
		state.execute();
	}
}
