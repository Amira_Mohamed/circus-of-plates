/*
 * 
 */
package eg.edu.alexu.csd.oop.game.state;

import javax.swing.JFrame;

import eg.edu.alexu.csd.oop.game.GameEngine;
import eg.edu.alexu.csd.oop.game.factory.Factory;
import eg.edu.alexu.csd.oop.game.singleton.Delegator;

/**
 * The Class Game.
 */
public class Game implements State {

	/** The delegator. */
	private Delegator delegator = Delegator.createInstance();

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.state.State#execute()
	 */
	@Override
	public void execute() {
		GameEngine.start("Candy Crush",
				new Factory().getStateObject(delegator.getLevelDifficulty(), delegator.getThemePath()),
				JFrame.EXIT_ON_CLOSE);
	}

}
