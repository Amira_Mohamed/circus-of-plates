/*
 * 
 */
package eg.edu.alexu.csd.oop.game.state;

/**
 * The Interface State.
 */
public interface State {

	/**
	 * Execute.
	 */
	public void execute();
}
