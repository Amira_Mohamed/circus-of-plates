/*
 * 
 */
package eg.edu.alexu.csd.oop.game.memento;

import java.util.LinkedList;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.observer.Observer;

/**
 * The Class CareTaker.
 */
public class CareTaker {

	/** The left list. */
	protected LinkedList<Memento> leftList = new LinkedList<Memento>();
	
	/** The right list. */
	protected LinkedList<Memento> rightList = new LinkedList<Memento>();
	
	/** The left size. */
	private int leftSize = leftList.size();
	
	/** The right size. */
	private int rightSize = rightList.size();

	/**
	 * Save left.
	 *
	 * @param memento the memento
	 */
	public void saveLeft(Memento memento) {
		leftList.add(memento);
		leftSize++;
	}

	/**
	 * Save right.
	 *
	 * @param memento the memento
	 */
	public void saveRight(Memento memento) {
		rightList.add(memento);
		rightSize++;
	}

	/**
	 * Check objects.
	 *
	 * @param side the side
	 * @return true, if successful
	 */
	public boolean checkObjects(String side) {
		if (side.equalsIgnoreCase("left") && leftSize >= 3) {
			String first = leftList.get(leftSize - 1).get().getClass().getSimpleName();
			String second = leftList.get(leftSize - 2).get().getClass().getSimpleName();
			String third = leftList.get(leftSize - 3).get().getClass().getSimpleName();
			return (first.equals(second) && first.equals(third));

		} else if (side.equalsIgnoreCase("right") && rightSize >= 3) {
			String first = rightList.get(rightSize - 1).get().getClass().getSimpleName();
			String second = rightList.get(rightSize - 2).get().getClass().getSimpleName();
			String third = rightList.get(rightSize - 3).get().getClass().getSimpleName();
			return (first.equals(second) && first.equals(third));
		}
		return false;
	}

	/**
	 * Check score.
	 *
	 * @param side the side
	 * @return the linked list
	 */
	public LinkedList<GameObject> checkScore(String side) {
		LinkedList<GameObject> list = new LinkedList<GameObject>();
		if (this.checkObjects(side)) {
			if (side.equalsIgnoreCase("left")) {
				for (int index = 0; index < 3; index++) {
					Observer object = (Observer) this.leftList.removeLast().get();
					list.add(object);
					leftSize--;
				}
			} else if (side.equalsIgnoreCase("right")) {
				for (int index = 0; index < 3; index++) {
					Observer object = (Observer) this.rightList.removeLast().get();
					list.add(object);
					rightSize--;
				}
			}
		}
		return list;
	}
}
