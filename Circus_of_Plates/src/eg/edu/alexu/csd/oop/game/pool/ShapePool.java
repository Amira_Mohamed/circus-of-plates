/*
 * 
 */
package eg.edu.alexu.csd.oop.game.pool;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.strategy.WorldImpl;

/**
 * The Class ShapePool.
 */
public class ShapePool {

	/** The classes. */
	private Map<String, Class<?>> classes;
	
	/** The Height. */
	private int Height = WorldImpl.HEIGHT / WorldImpl.FACTOR;
	
	/** The x position. */
	private int[] xPosition = { -32, WorldImpl.WIDTH };
	
	/** The y position. */
	private int[] yPosition = { Height, Height };
	
	/** The shape pool. */
	private static ShapePool shapePool = new ShapePool();
	
	/** The map. */
	private Map<String, LinkedList<GameObject>> map = new LinkedHashMap<String, LinkedList<GameObject>>();
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(ShapePool.class);
	
	/** The rate of creation. */
	private int rateOfCreation = 0;

	/**
	 * Instantiates a new shape pool.
	 */
	private ShapePool() {
		this.map.put("BlueShape", new LinkedList<GameObject>());
		this.map.put("GreenShape", new LinkedList<GameObject>());
		this.map.put("RedShape", new LinkedList<GameObject>());
		this.map.put("YellowShape", new LinkedList<GameObject>());
	}

	/**
	 * Gets the shape.
	 *
	 * @param objectType the object type
	 * @return the shape
	 */
	public GameObject getShape(String objectType) {
		this.rateOfCreation++;
		LinkedList<GameObject> objectList = map.get(objectType);
		if (objectList.size() == 0) {
			try {
				GameObject object = (GameObject) classes.get(objectType).newInstance();
				logger.info(">> GameObject created Successfully");
				this.intialize(object);
				return object;
			} catch (InstantiationException | IllegalAccessException e) {
				logger.fatal(">> GameObject Can't be created " + e);
				throw new RuntimeException();
			}
		} else {
			try {
				GameObject object = objectList.getFirst();
				objectList.remove(object);
				this.intialize(object);
				return object;
			} catch (RuntimeException e) {
				logger.fatal(">> GameObject Can't be created " + e);
				throw new RuntimeException();
			}
		}
	}

	/**
	 * Release shape.
	 *
	 * @param object the object
	 */
	public void releaseShape(GameObject object) {
		String className = object.getClass().getSimpleName();
		map.get(className).add(object);
		return;
	}

	/**
	 * Intialize.
	 *
	 * @param object the object
	 */
	private void intialize(GameObject object) {
		object.setX(xPosition[rateOfCreation % 2]);
		object.setY(yPosition[rateOfCreation % 2] - object.getHeight());
		return;
	}

	/**
	 * Gets the single instance of ShapePool.
	 *
	 * @return single instance of ShapePool
	 */
	public static ShapePool getInstance() {
		return shapePool;
	}

	/**
	 * Sets the classes.
	 *
	 * @param map the map
	 */
	public void setClasses(Map<String, Class<?>> map) {
		classes = map;
	}
}
