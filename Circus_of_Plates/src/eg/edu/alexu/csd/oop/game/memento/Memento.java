/*
 * 
 */
package eg.edu.alexu.csd.oop.game.memento;

import eg.edu.alexu.csd.oop.game.GameObject;

/**
 * The Class Memento.
 */
public class Memento {

	/** The game object. */
	private GameObject gameObject;

	/**
	 * Instantiates a new memento.
	 *
	 * @param gameObject the game object
	 */
	public Memento(GameObject gameObject) {
		this.gameObject = gameObject;
	}

	/**
	 * Gets the.
	 *
	 * @return the game object
	 */
	protected GameObject get() {
		return this.gameObject;
	}

}
