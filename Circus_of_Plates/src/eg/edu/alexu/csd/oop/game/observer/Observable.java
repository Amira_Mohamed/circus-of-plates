/*
 * 
 */
package eg.edu.alexu.csd.oop.game.observer;

/**
 * The Interface Observable.
 */
public interface Observable {
	
	/**
	 * Adds the observer.
	 *
	 * @param observer the observer
	 */
	void addObserver(Observer observer);

	/**
	 * Notify all observers.
	 */
	void notifyAllObservers();
}
