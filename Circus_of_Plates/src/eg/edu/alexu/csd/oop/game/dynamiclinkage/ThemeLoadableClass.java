/*
 * 
 */
package eg.edu.alexu.csd.oop.game.dynamiclinkage;

import java.util.Map;

import eg.edu.alexu.csd.oop.game.World;

/**
 * The Class ThemeLoadableClass.
 */
public abstract class ThemeLoadableClass {
	
	/** The world. */
	protected World world;
	
	/** The map. */
	protected Map<String, Class<?>> map;

	/**
	 * Sets the environment.
	 *
	 * @param world the new environment
	 */
	public abstract void setEnvironment(World world);

	/**
	 * Start.
	 */
	public abstract void start();

	/**
	 * Sets the classes.
	 *
	 * @param map the map
	 */
	public abstract void setClasses(Map<String, Class<?>> map);

	/**
	 * Gets the clowns number.
	 *
	 * @return the clowns number
	 */
	public abstract int getClownsNumber();
}
