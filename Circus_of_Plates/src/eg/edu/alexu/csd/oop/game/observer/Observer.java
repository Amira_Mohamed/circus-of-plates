/*
 * 
 */
package eg.edu.alexu.csd.oop.game.observer;

import eg.edu.alexu.csd.oop.game.GameObject;

/**
 * The Interface Observer.
 */
public interface Observer extends GameObject {
	
	/**
	 * Update.
	 */
	void update();
}
