/*
 * 
 */
package eg.edu.alexu.csd.oop.game.objects;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.memento.CareTaker;
import eg.edu.alexu.csd.oop.game.memento.Originator;

/**
 * The Interface CharacterIF.
 */
public interface CharacterIF extends GameObject {

	/**
	 * Gets the left stack y position.
	 *
	 * @return the left stack y position
	 */
	public int getLeftStackYPosition();

	/**
	 * Gets the right stack y position.
	 *
	 * @return the right stack y position
	 */
	public int getRightStackYPosition();

	/**
	 * Gets the care taker.
	 *
	 * @return the care taker
	 */
	public CareTaker getCareTaker();

	/**
	 * Gets the originator.
	 *
	 * @return the originator
	 */
	public Originator getOriginator();

	/**
	 * Sets the left stack y position.
	 *
	 * @param leftStackYPosition the new left stack y position
	 */
	public void setLeftStackYPosition(int leftStackYPosition);

	/**
	 * Sets the right stack y position.
	 *
	 * @param rightStackYPosition the new right stack y position
	 */
	public void setRightStackYPosition(int rightStackYPosition);
}
