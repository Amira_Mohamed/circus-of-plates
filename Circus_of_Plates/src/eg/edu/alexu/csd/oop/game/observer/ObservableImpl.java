/*
 * 
 */
package eg.edu.alexu.csd.oop.game.observer;

import java.util.LinkedList;

/**
 * The Class ObservableImpl.
 */
public class ObservableImpl implements Observable {

	/** The observer. */
	private LinkedList<Observer> observer = new LinkedList<Observer>();

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.observer.Observable#notifyAllObservers()
	 */
	@Override
	public void notifyAllObservers() {
		for (Observer itr : observer) {
			itr.update();
		}
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.observer.Observable#addObserver(eg.edu.alexu.csd.oop.game.observer.Observer)
	 */
	@Override
	public void addObserver(Observer observer) {
		this.observer.add(observer);
	}

}
