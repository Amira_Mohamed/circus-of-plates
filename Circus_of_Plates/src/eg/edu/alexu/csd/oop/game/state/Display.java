/*
 * 
 */
package eg.edu.alexu.csd.oop.game.state;

import java.awt.Color;
import java.awt.FileDialog;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.singleton.Delegator;

/**
 * The Class Display.
 */
public class Display implements Runnable {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Display.class);
	
	/** The Level difficulty. */
	private String LevelDifficulty = null;
	
	/** The path. */
	private String path = null;
	
	/** The delegator. */
	private Delegator delegator = Delegator.createInstance();

	/** The frame. */
	public JFrame frame;
	
	/** The exit. */
	private JButton option, exit;
	
	/** The label. */
	private JLabel label;
	
	/** The difficulty. */
	private String[] difficulty = { "Easy", "Medium", "Hard" };

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		frame = new JFrame("Candy Crush");
		frame.setBounds(0, 0, 900, 680);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		exit = new JButton("Exit");
		exit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		exit.setForeground(new Color(128, 0, 128));
		exit.setFont(new Font("Buxton Sketch", Font.BOLD, 20));
		exit.setBackground(new Color(255, 255, 224));
		exit.setBounds(350, 475, 200, 50);
		frame.getContentPane().add(exit);

		for (int i = 0; i < 3; i++) {
			addAction(i);
		}

		label = new JLabel("");
		Image img = new ImageIcon(this.getClass().getResource("/candy.jpg")).getImage();
		label.setIcon(new ImageIcon(img));
		label.setBounds(0, 0, 900, 680);
		frame.getContentPane().add(label);
		frame.setVisible(true);
	}

	/**
	 * Adds the action.
	 *
	 * @param i the i
	 */
	private void addAction(final int i) {
		option = new JButton(difficulty[i]);
		option.setForeground(new Color(128, 0, 128));
		option.setFont(new Font("Buxton Sketch", Font.BOLD, 20));
		option.setBounds(350, 250 + (i * 75), 200, 50);
		option.setBackground(new Color(255, 255, 224));
		option.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				LevelDifficulty = difficulty[i];
				delegator.setLevelDifficulty(LevelDifficulty);
				logger.info(">> User has choosed to play " + LevelDifficulty + " game");
				FileDialog dialog = new FileDialog(frame, "Choose Theme", FileDialog.LOAD);
				JOptionPane.showMessageDialog(null, "Choose Theme");
				dialog.setVisible(true);
				path = dialog.getDirectory() + dialog.getFile();
				delegator.setThemePath(path);
				logger.info(">> User selected the path Successfully  " + path);
			}
		});
		frame.add(option);
	}

	/**
	 * Dispose frame.
	 */
	public void disposeFrame() {
		frame.dispose();
	}

}
