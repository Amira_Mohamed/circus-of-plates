/*
 * 
 */
package eg.edu.alexu.csd.oop.game.state;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.singleton.Delegator;

/**
 * The Class Intro.
 */
public class Intro implements State {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Intro.class);

	/** The delegator. */
	private Delegator delegator = Delegator.createInstance();

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.state.State#execute()
	 */
	@Override
	public void execute() {
		Display display = new Display();
		Thread thread = new Thread(display);
		thread.start();
		try {
			do {
				Thread.currentThread();
				Thread.sleep(50);
			} while (delegator.getThemePath() == null);
			thread.join();
		} catch (InterruptedException e) {
			logger.fatal(">> Thread interruption error Occured\n" + e);
		}
		display.disposeFrame();

	}

}
