/*
 * 
 */
package eg.edu.alexu.csd.oop.game.launcher;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.state.Game;
import eg.edu.alexu.csd.oop.game.state.Intro;
import eg.edu.alexu.csd.oop.game.state.View;

/**
 * The Class Engine.
 */
public class Engine {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Engine.class);

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		View view = new View();
		try {
			view.setState(new Intro());
			logger.info(">> Intro State started Successfully");
			view.start();
		} catch (RuntimeException e) {
			logger.fatal(">> Intro Object can't be created" + e);
		}
		logger.info(">> Game State started Successfully");
		try {
			view.setState(new Game());
			view.start();
		} catch (RuntimeException e) {
			logger.fatal(">> Game Object can't be created" + e);
		}
	}
}
