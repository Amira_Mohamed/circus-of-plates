/*
 * 
 */
package eg.edu.alexu.csd.oop.game.dynamiclinkage;

import java.awt.image.BufferedImage;

import eg.edu.alexu.csd.oop.game.flyweight.ImageFactory;
import eg.edu.alexu.csd.oop.game.observer.Observer;

/**
 * The Class Shape.
 */
public class Shape implements Observer {
	
	/** The sprite images. */
	// an array of sprite images that are drawn sequentially
	protected BufferedImage[] spriteImages = new BufferedImage[1];
	
	/** The shapes. */
	protected ImageFactory shapes = new ImageFactory();

	/** The x. */
	// private int randomIndex;
	private int x;
	
	/** The y. */
	private int y;
	
	/** The visible. */
	protected boolean visible = true;

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#getX()
	 */
	@Override
	public int getX() {
		return x;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#setX(int)
	 */
	@Override
	public void setX(int mX) {
		this.x = mX;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#getY()
	 */
	@Override
	public int getY() {
		return y;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#setY(int)
	 */
	@Override
	public void setY(int mY) {
		this.y = mY;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#getSpriteImages()
	 */
	@Override
	public BufferedImage[] getSpriteImages() {
		return spriteImages;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#getWidth()
	 */
	@Override
	public int getWidth() {
		return spriteImages[0].getWidth();
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#getHeight()
	 */
	@Override
	public int getHeight() {
		return spriteImages[0].getHeight();
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.GameObject#isVisible()
	 */
	@Override
	public boolean isVisible() {
		return visible;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.observer.Observer#update()
	 */
	@Override
	public void update() {
		this.visible = !visible;
	}

}
