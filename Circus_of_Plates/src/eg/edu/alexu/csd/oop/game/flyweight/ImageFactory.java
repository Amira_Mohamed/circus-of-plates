/*
 * 
 */
package eg.edu.alexu.csd.oop.game.flyweight;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

/**
 * A factory for creating Image objects.
 */
public class ImageFactory {

	/** The map. */
	private HashMap<String, BufferedImage> map = new HashMap<String, BufferedImage>();

	/**
	 * Gets the shape.
	 *
	 * @param path the path
	 * @return the shape
	 */
	public BufferedImage getShape(String path) {
		BufferedImage img = map.get(path);
		if (img == null) {
			try {
				img = ImageIO.read(getClass().getResourceAsStream(path));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			map.put(path, img);
		}
		return img;
	}

}
