/*
 * 
 */
package eg.edu.alexu.csd.oop.game.singleton;

/**
 * The Class Delegator.
 */
public class Delegator {

	/** The level difficulty. */
	private String levelDifficulty = null ;
	
	/** The theme path. */
	private String themePath = null ;
	
	/** The delegator. */
	private static Delegator delegator = new Delegator() ;
	
	/**
	 * Instantiates a new delegator.
	 */
	private Delegator(){}
	
	/**
	 * Creates the instance.
	 *
	 * @return the delegator
	 */
	public static Delegator createInstance(){
		return delegator ;
	}
	
	/**
	 * Gets the level difficulty.
	 *
	 * @return the level difficulty
	 */
	public String getLevelDifficulty() {
		return levelDifficulty ;
	}
	
	/**
	 * Sets the level difficulty.
	 *
	 * @param levelDifficulty the new level difficulty
	 */
	public void setLevelDifficulty(String levelDifficulty) {
		this.levelDifficulty = levelDifficulty ;
	}
	
	/**
	 * Gets the theme path.
	 *
	 * @return the theme path
	 */
	public String getThemePath() {
		return themePath ;
	}
	
	/**
	 * Sets the theme path.
	 *
	 * @param themePath the new theme path
	 */
	public void setThemePath(String themePath) {
		this.themePath = themePath ;
	}
	
}
