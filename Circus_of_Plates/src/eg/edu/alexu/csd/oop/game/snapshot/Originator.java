/*
 * 
 */
package eg.edu.alexu.csd.oop.game.snapshot;

import eg.edu.alexu.csd.oop.game.GameObject;

/**
 * The Class Originator.
 */
public class Originator {
	
	/**
	 * Save.
	 *
	 * @param gameObject the game object
	 * @return the memento
	 */
	public Memento save(GameObject gameObject) {
		return new Memento(gameObject);
	}

}
