/*
 * 
 */
package eg.edu.alexu.csd.oop.game;

// TODO: Auto-generated Javadoc
/**
 * The Interface World.
 */
public interface World {
	
	/**
	 * Gets the constant objects.
	 *
	 * @return list of immovable object
	 */
	java.util.List<GameObject> getConstantObjects();

	/**
	 * Gets the movable objects.
	 *
	 * @return list of moving object
	 */
	java.util.List<GameObject> getMovableObjects();

	/**
	 * Gets the controlable objects.
	 *
	 * @return list of user controlled object
	 */
	java.util.List<GameObject> getControlableObjects();

	/**
	 * Gets the width.
	 *
	 * @return screen width
	 */
	int getWidth();

	/**
	 * Gets the height.
	 *
	 * @return screen height
	 */
	int getHeight();

	/**
	 * refresh the world state and update locations.
	 *
	 * @return false means game over
	 */
	boolean refresh();

	/**
	 * status bar content.
	 *
	 * @return string to be shown at status bar
	 */
	String getStatus();

	/**
	 * Gets the speed.
	 *
	 * @return frequency of calling refresh
	 */
	int getSpeed();

	/**
	 * Gets the control speed.
	 *
	 * @return frequency of receiving user input
	 */
	int getControlSpeed();
}