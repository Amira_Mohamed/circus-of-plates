/*
 * 
 */
package eg.edu.alexu.csd.oop.game.strategy;

/**
 * The Class Medium.
 */
public class Medium extends Easy {

	/**
	 * Instantiates a new medium.
	 *
	 * @param path the path
	 */
	public Medium(String path) {
		super(path);
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.Easy#getSpeed()
	 */
	@Override
	public int getSpeed() {
		return 15;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.Easy#getControlSpeed()
	 */
	@Override
	public int getControlSpeed() {
		return 25;
	}
}
