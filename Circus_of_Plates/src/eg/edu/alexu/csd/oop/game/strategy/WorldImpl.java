/*
 * 
 */
package eg.edu.alexu.csd.oop.game.strategy;

import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.World;
import eg.edu.alexu.csd.oop.game.dynamiclinkage.ThemeLoadableClass;
import eg.edu.alexu.csd.oop.game.iterator.Iterator;
import eg.edu.alexu.csd.oop.game.iterator.IteratorImpl;
import eg.edu.alexu.csd.oop.game.objects.CharacterIF;
import eg.edu.alexu.csd.oop.game.pool.ShapePool;

/**
 * The Class WorldImpl.
 */
public class WorldImpl implements World {

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(WorldImpl.class);
	
	/** The Constant WIDTH. */
	public static final int WIDTH = 900;
	
	/** The Constant HEIGHT. */
	public static final int HEIGHT = 680;
	
	/** The Constant PIC_DIMENSION. */
	public static final int PIC_DIMENSION = 32;
	
	/** The Constant FACTOR. */
	public static final int FACTOR = 4;
	
	/** The Constant MAX_TIME. */
	protected static final int MAX_TIME = 60 * 1000; // 1 minute
	
	/** The score. */
	protected int score = 0;
	
	/** The start time. */
	protected long startTime = System.currentTimeMillis();
	
	/** The constant. */
	protected final List<GameObject> constant = new LinkedList<GameObject>();
	
	/** The moving. */
	protected final List<GameObject> moving = new LinkedList<GameObject>();
	
	/** The control. */
	protected final List<GameObject> control = new LinkedList<GameObject>();
	
	/** The pool. */
	protected ShapePool pool;
	
	/** The clowns number. */
	protected int clownsNumber;
	
	/** The random shape generator. */
	protected String[] randomShapeGenerator = { "BlueShape", "GreenShape", "YellowShape", "RedShape" };
	
	/** The bar1. */
	protected GameObject bar1;
	
	/** The bar2. */
	protected GameObject bar2;
	
	/** The target score. */
	protected int targetScore;
	
	/** The iterator. */
	protected IteratorImpl iterator;
	
	/** The player state. */
	protected String playerState = "";

	/**
	 * Instantiates a new world impl.
	 */
	public WorldImpl() {
		pool = ShapePool.getInstance();
	}

	/**
	 * Sets the bar shapes.
	 */
	public void setBarShapes() {
		int shift = 0;
		for (int i = 0; i < 7; i++) {
			GameObject object1 = pool.getShape(randomShapeGenerator[new Random().nextInt(4)]);
			logger.debug(">> GameObject " + object1.getClass().getName() + " is created Successfully");
			GameObject object2 = pool.getShape(randomShapeGenerator[new Random().nextInt(4)]);
			logger.debug(">> GameObject " + object2.getClass().getName() + " is created Successfully");
			object1.setX(object1.getX() - shift);
			object2.setX(object2.getX() + shift);
			this.moving.add(object1);
			this.moving.add(object2);
			shift += object1.getWidth();
		}
	}

	/**
	 * Process jar.
	 *
	 * @param path the path
	 * @return the map
	 * @throws MalformedURLException the malformed url exception
	 */
	@SuppressWarnings("resource")
	public Map<String, Class<?>> processJar(String path) throws MalformedURLException {
		Map<String, Class<?>> map = new HashMap<String, Class<?>>();
		try {
			File file = new File(path);
			URLClassLoader loader = new URLClassLoader(new URL[] { file.toURI().toURL() },
					ClassLoader.getSystemClassLoader());
			JarInputStream inputStream = new JarInputStream(new FileInputStream(path));
			JarEntry entry;
			while (true) {
				entry = inputStream.getNextJarEntry();
				if (entry == null)
					break;
				if (entry.getName().endsWith(".class")) {
					String x = entry.getName().replaceAll("/", ".");
					x = x.split(".class")[0];
					String className = entry.getName().substring(0, entry.getName().length() - 6);
					className = className.replace('/', '.');
					try {
						Class<?> classLoader = loader.loadClass(className);
						map.put(classLoader.getSimpleName(), classLoader);
						logger.info(">> " + classLoader.getSimpleName());
					} catch (Exception ex) {
						logger.fatal(">> ClassLoader Exception " + ex);
					}
				}
			}
		} catch (Exception ex) {
			logger.fatal(">> URLClassLoader Exception " + ex);
		}
		return map;
	}

	/**
	 * Run.
	 *
	 * @param path the path
	 */
	protected void run(String path) {
		Map<String, Class<?>> map = null;
		try {
			map = processJar(path);
		} catch (MalformedURLException e) {
			logger.fatal(e);
		}
		ThemeLoadableClass theme = null;
		try {
			theme = (ThemeLoadableClass) map.get("Theme").newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			logger.fatal(e);
		}
		theme.setEnvironment(this);
		theme.setClasses(map);
		pool.setClasses(map);
		theme.start();
		this.clownsNumber = theme.getClownsNumber();
		map.remove("Theme");
	}

	/**
	 * Intersect bar.
	 *
	 * @param object the object
	 * @return the game object
	 */
	protected GameObject IntersectBar(GameObject object) {
		Iterator iterator = new IteratorImpl(this.constant);
		iterator.next();
		while (iterator.hasNext()) {
			GameObject bar = iterator.next();
			logger.debug(">> Bar GameObject is created Successfully");
			if (((object.getHeight() + object.getY()) == bar.getY()) && ((isLeft(bar) && object.getX() < bar.getWidth())
					|| (isRight(bar) && object.getX() > bar.getX() - 20))) {
				logger.debug(">> " + object.getClass().getSimpleName() + " intersects bar");
				return bar;
			}
		}
		return null;
	}

	/**
	 * Intersect clown.
	 *
	 * @param object the object
	 * @return the character if
	 */
	protected CharacterIF intersectClown(GameObject object) {
		for (int index = 0; index < this.clownsNumber; index++) {
			CharacterIF clown = (CharacterIF) this.control.get(index);
			logger.debug(">> Clown GameObject is created Successfully");
			if (getClownLeftHand(clown).intersects(getShape(object))) {
				object.setX(clown.getX());
				object.setY(clown.getLeftStackYPosition());
				logger.debug(">> " + object.getClass().getSimpleName() + " intersects Clown's left Hand");
				return clown;
			} else if (getClownRightHand(clown).intersects(getShape(object))) {
				object.setX(clown.getX() + 3 * (clown.getWidth() / 4) - FACTOR * (FACTOR + 3));
				object.setY(clown.getRightStackYPosition());
				logger.debug(">> " + object.getClass().getSimpleName() + " intersects Clown's right Hand");
				return clown;
			}
		}
		return null;
	}

	/**
	 * Reach bar.
	 *
	 * @param clown the clown
	 * @return true, if successful
	 */
	protected boolean reachBar(GameObject clown) {
		CharacterIF object = (CharacterIF) clown;
		return (object.getRightStackYPosition() <= this.bar1.getY()
				&& object.getLeftStackYPosition() <= this.bar1.getY());
	}

	/**
	 * Gets the clown left hand.
	 *
	 * @param clown the clown
	 * @return the clown left hand
	 */
	protected Rectangle getClownLeftHand(CharacterIF clown) {
		return new Rectangle(clown.getX(), clown.getLeftStackYPosition(), clown.getWidth() / FACTOR, 1);
	}

	/**
	 * Gets the clown right hand.
	 *
	 * @param clown the clown
	 * @return the clown right hand
	 */
	protected Rectangle getClownRightHand(CharacterIF clown) {
		return new Rectangle(clown.getX() + 3 * (clown.getWidth() / 4)- FACTOR * (FACTOR+3), clown.getRightStackYPosition(),
				clown.getWidth() / FACTOR, 1);
	}

	/**
	 * Gets the shape.
	 *
	 * @param object the object
	 * @return the shape
	 */
	protected Rectangle getShape(GameObject object) {
		return new Rectangle(object.getX(), object.getY(), object.getWidth(), object.getHeight());
	}

	/**
	 * Checks if is left.
	 *
	 * @param bar the bar
	 * @return true, if is left
	 */
	protected boolean isLeft(GameObject bar) {
		return (bar.getX() == 0);
	}

	/**
	 * Checks if is right.
	 *
	 * @param bar the bar
	 * @return true, if is right
	 */
	protected boolean isRight(GameObject bar) {
		return (bar.getX() != 0);
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getConstantObjects()
	 */
	@Override
	public List<GameObject> getConstantObjects() {
		return this.constant;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getMovableObjects()
	 */
	@Override
	public List<GameObject> getMovableObjects() {
		return this.moving;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getControlableObjects()
	 */
	@Override
	public List<GameObject> getControlableObjects() {
		return this.control;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getWidth()
	 */
	@Override
	public int getWidth() {
		return WIDTH;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getHeight()
	 */
	@Override
	public int getHeight() {
		return HEIGHT;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#refresh()
	 */
	@Override
	public boolean refresh() {
		return false;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getStatus()
	 */
	@Override
	public String getStatus() {
		return null;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getSpeed()
	 */
	@Override
	public int getSpeed() {
		return 20;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.World#getControlSpeed()
	 */
	@Override
	public int getControlSpeed() {
		return 10;
	}
}
