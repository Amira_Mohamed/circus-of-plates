/*
 * 
 */
package eg.edu.alexu.csd.oop.game.strategy;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;

import eg.edu.alexu.csd.oop.game.GameObject;
import eg.edu.alexu.csd.oop.game.iterator.IteratorImpl;
import eg.edu.alexu.csd.oop.game.objects.CharacterIF;
import eg.edu.alexu.csd.oop.game.observer.BombShape;
import eg.edu.alexu.csd.oop.game.observer.ObservableImpl;
import eg.edu.alexu.csd.oop.game.observer.Observer;
import eg.edu.alexu.csd.oop.game.snapshot.CareTaker;
import eg.edu.alexu.csd.oop.game.snapshot.Memento;
import eg.edu.alexu.csd.oop.game.snapshot.Originator;

/**
 * The Class Hard.
 */
public class Hard extends WorldImpl {
	
	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(Easy.class);
	
	/** The care taker. */
	private CareTaker careTaker;
	
	/** The originator. */
	private Originator originator;
	
	/** The observer. */
	private ObservableImpl observer;
	
	/** The Bomb. */
	private boolean Bomb = false;
	
	/** The save current. */
	private boolean saveCurrent = false;
	
	/** The load current. */
	private boolean loadCurrent = false;

	/**
	 * Instantiates a new hard.
	 *
	 * @param path the path
	 */
	public Hard(String path) {
		super();
		run(path);
		setBarShapes();
		this.bar1 = constant.get(0);
		this.bar2 = constant.get(1);
		this.observer = new ObservableImpl();
		this.careTaker = new CareTaker();
		this.originator = new Originator();
		for (int i = 0; i < 10; i++) {
			GameObject object = new BombShape();
			observer.addObserver((Observer) object);
			this.moving.add(object);
		}
		observer.notifyAllObservers();
		super.targetScore = 25;
	}

	/**
	 * Checks if is bomb.
	 *
	 * @param object the object
	 * @return true, if is bomb
	 */
	public boolean isBomb(GameObject object) {
		if (object.getClass().getSimpleName().equals("BombShape")) {
			logger.info("GameObject is a Bomb");
		}
		return object.getClass().getSimpleName().equals("BombShape");
	}

	/**
	 * Go to prev state.
	 */
	private void goToPrevState() {
		control.clear();
		LinkedList<Memento> memento = careTaker.load();
		for (Memento object : memento) {
			GameObject gameObject = object.get();
			if (gameObject instanceof CharacterIF) {
				CharacterIF obj = (CharacterIF) gameObject;
				obj.setRightStackYPosition(object.getRight());
				obj.setLeftStackYPosition(object.getLeft());
				this.score = object.getScore();
			}
			control.add(gameObject);
		}
	}

	/**
	 * Save current state.
	 */
	private void saveCurrentState() {
		for (GameObject object : control) {
			if (object instanceof CharacterIF) {
				CharacterIF obj = (CharacterIF) object;
				Memento memento = originator.save(object);
				memento.setRight(obj.getRightStackYPosition());
				memento.setLeft(obj.getLeftStackYPosition());
				memento.setX(obj.getX());
				memento.setScore(this.score);
				careTaker.save(memento);
			} else {
				careTaker.save(originator.save(object));
			}
		}
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#refresh()
	 */
	@Override
	public boolean refresh() {
		long now = System.currentTimeMillis();
		boolean clownReachedBar = true;
		List<GameObject> moveableToControl = new LinkedList<GameObject>();
		List<GameObject> removedMovingShapes = new LinkedList<GameObject>();
		iterator = new IteratorImpl(moving);
		while (iterator.hasNext()) {
			GameObject object = iterator.next();
			if (!object.isVisible())
				continue;
			GameObject bar = IntersectBar(object);
			if (bar != null && !isBomb(object)) {
				if (isLeft(bar)) {
					object.setX(object.getX() + 1);
					if (IntersectBar(object) == null) {
						moving.add(pool.getShape(randomShapeGenerator[new Random().nextInt(4)]));
					}
				} else {
					object.setX(object.getX() - 1);
					if (IntersectBar(object) == null) {
						moving.add(pool.getShape(randomShapeGenerator[new Random().nextInt(4)]));
					}
				}
			} else {
				CharacterIF clown = intersectClown(object);
				if (isBomb(object)) {
					if (clown != null && !loadCurrent) {
						loadCurrent = true;
						this.goToPrevState();
					}
					object.setY(object.getY() + 1);
				} else if (clown == null) {
					object.setY(object.getY() + 4);
					if (object.getY() > HEIGHT) {
						super.pool.releaseShape(object);
						removedMovingShapes.add(object);
					}
				} else {
					if (getClownLeftHand(clown).intersects(getShape(object))) {
						clown.getCareTaker().saveLeft(clown.getOriginator().save(object));
						LinkedList<GameObject> list = clown.getCareTaker().checkScore("left");
						if (list.size() != 0) {
							super.score++;
							clown.setLeftStackYPosition(clown.getLeftStackYPosition() + 2 * object.getHeight());
							for (GameObject dummy : list) {
								if (dummy != object) {
									super.control.remove(dummy);
								}
							}
							super.pool.releaseShape(object);
							removedMovingShapes.add(object);
						} else {
							clown.setLeftStackYPosition(clown.getLeftStackYPosition() - object.getHeight());
							logger.info(object.getClass().getSimpleName() + "is turned into Control");
							moveableToControl.add(object);
						}
					} else if (getClownRightHand(clown).intersects(getShape(object))) {
						clown.getCareTaker().saveRight(clown.getOriginator().save(object));
						LinkedList<GameObject> list = clown.getCareTaker().checkScore("right");
						if (list.size() != 0) {
							super.score++;
							clown.setRightStackYPosition(clown.getRightStackYPosition() + 2 * object.getHeight());
							for (GameObject dummy : list) {
								if (dummy != object) {
									super.control.remove(dummy);
								}
							}
							super.pool.releaseShape(object);
							removedMovingShapes.add(object);
						} else {
							clown.setRightStackYPosition(clown.getRightStackYPosition() - object.getHeight());
							moveableToControl.add(object);
						}
					}
				}
			}
		}
		for (GameObject object : moveableToControl) {
			super.control.add(object);
			super.moving.remove(object);
		}
		for (GameObject object : removedMovingShapes) {
			moving.remove(object);
		}
		for (int i = 0; i < super.clownsNumber; i++) {
			clownReachedBar = clownReachedBar && super.reachBar(super.control.get(i));
		}

		if (now - super.startTime >= MAX_TIME / 10 && !saveCurrent) {
			this.saveCurrent = true;
			this.saveCurrentState();
		}

		if (now - super.startTime >= MAX_TIME / 4 && !Bomb) {
			observer.notifyAllObservers();
			Bomb = true;
		}
		if (super.score == targetScore) {
			super.playerState = "Winner";
		}
		if ((now - super.startTime < MAX_TIME) && !(clownReachedBar)) {
			return true;
		} else {
			super.playerState = "Looser";
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#getSpeed()
	 */
	@Override
	public int getSpeed() {
		return 15;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#getControlSpeed()
	 */
	@Override
	public int getControlSpeed() {
		return 25;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.game.strategy.WorldImpl#getStatus()
	 */
	@Override
	public String getStatus() {
		return "Score = " + this.score + "   | |   Target Score = " + targetScore + "   | |   Timer = "
				+ (System.currentTimeMillis() - startTime) / 1000 + " From " + MAX_TIME / 1000;
	}

}
